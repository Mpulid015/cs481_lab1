﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab1
{
    public partial class MainPage : ContentPage
    {
        private string mood;
        public string TextMood
        {
              get { return mood; }
            set
            {
                mood = value;
                OnPropertyChanged(nameof(TextMood));
            }
        }
       
        public MainPage()
        {
            InitializeComponent();
            BindingContext = this;
            //this.LabelColor = "#0B6623";
            Master.BackgroundColor = Xamarin.Forms.Color.Aquamarine;
            this.TextMood = "Happy Text";
        }
        public void ChangeButton(Object sender, EventArgs e)
        {
            if (Master.BackgroundColor == Xamarin.Forms.Color.Aquamarine)
            {
                Master.BackgroundColor = Xamarin.Forms.Color.ForestGreen;
                this.TextMood = "Not good";


            }
            else if(Master.BackgroundColor == Xamarin.Forms.Color.ForestGreen)
            {
                Master.BackgroundColor = Xamarin.Forms.Color.DarkRed;
                this.TextMood = "Angry";
            }
            else if ( Master.BackgroundColor == Xamarin.Forms.Color.DarkRed)
            {
                Master.BackgroundColor = Xamarin.Forms.Color.Aquamarine;
                this.TextMood = "Happy Text";
            }

        }
    }
}
